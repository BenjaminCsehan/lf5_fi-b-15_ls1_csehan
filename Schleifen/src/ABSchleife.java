
public class ABSchleife

{
    public static void main(String[] args)
    {
        System.out.println(summe(5));
    }

    public static int summe(int n)
    {

        int summe = 0;
        for (int i = 1; i <= n; i++)
        {
            summe = summe + i;
        }
        return summe;
    }

}

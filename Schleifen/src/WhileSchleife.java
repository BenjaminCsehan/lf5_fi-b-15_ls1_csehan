
public class WhileSchleife {

	public static void main(String[] args) {
		
		int a = 0;
		   System.out.println();
		 
		   while (a < 10)
		   {
		      System.out.println("Schleifendurchlauf " + a);
		      if (a == 10) System.out.println();
		      a++;
	}

	}
}

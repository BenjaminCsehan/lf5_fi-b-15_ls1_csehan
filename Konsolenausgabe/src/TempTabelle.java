public class TempTabelle {

	public static void main(String[] args) {
		
		System.out.println("\n Aufgabe 3: \n ");
		
		System.out.printf("%-12s|%10s %n", "Fahrenheit", "Celsius");
		System.out.printf("------------------------- \n");
		System.out.printf("%-12s|%10.6s %n", "-20", "-28.8889");
		System.out.printf("%-12s|%10.6s %n", "-20", "-23.3333");
		System.out.printf("%-12s|%10.6s %n", "-20", "-17.7778");
		System.out.printf("%-12s|%10.6s %n", "-20", "-6.6667");
		System.out.printf("%-12s|%10.6s %n", "-20", "-1.1111");
		
    
	}
}